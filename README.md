# Phone

### Controlling SuperCollider with a Phone

This class contains classes for using a phone to control stuff in SuperCollider, written for a series of workshops on integrating sound and movement. In order to use this, install any app that gives access to the phone's accelerometers and transmits via OSC. We use TouchOSC, but there are other alternatives as well.

### Installation

If you have Git installed on your system (recommended):

Open up SuperCollider and evaluate the following line of code:
```supercollider 
Quarks.install("https://gitlab.com/kflak/phone");
```

Recompile SuperCollider by going to `Language->Recompile Class Library`
in the SuperCollider IDE.

If you _don't_ have Git installed on your system:

Download the code as a Zip archive by clicking the Download button (to
the left of the blue Clone button) and click on zip. This will place an
archive named `phone-main.zip` into your Downloads folder.

Next: Find SuperCollider's extensions folder by evaluating

```supercollider
Platform.userExtensionDir;
```

On Linux, this will return something like 
```
-> /home/kf/.local/share/SuperCollider/Extensions
```

Unzip `phone-main.zip` into this folder.

Recompile SuperCollider by going to `Language->Recompile Class Library`
in the SuperCollider IDE.
