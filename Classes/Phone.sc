Phone {
    classvar <>resamplingFreq = 20;
    classvar <>oscPath = '/accxyz';

    var <>brand;
    var <>port;
    var <x=0, <y=0, <z=0, <delta=0;
    var minValue;
    var maxValue;
    var oscFunc;
    var oscFuncTrace;
    var <data, prevData;
    var task;
    var <>server;

    *new {|brand=\iPhone, port=55555|
        ^super.newCopyArgs(brand, port).init;
    }

    init {
        server = server ?? {Server.default};
        data = 0.0 ! 3;
        prevData = 0.0 ! 3;

        minValue = (
            iPhone: -1.0,
            android: -10.0,
            );
        maxValue = (
            iPhone: 1.0,
            android: 10.0,
            );
        thisProcess.openUDPPort(port);
        this.prCreateOscFunc;
        this.prCreateTask;
    }

    prCreateOscFunc {
        oscFunc = OSCFunc({|oscdata|
            data = this.prNormalizeData(oscdata[1..3]);
            x = data[0];
            y = data[1];
            z = data[2];
        }, path: oscPath, recvPort: port);
    }

    prNormalizeData {|data|
        var normalizedData = data.linlin(
            minValue[brand], maxValue[brand], 0.0, 1.0
        );
        ^normalizedData;
    }

    prCalculateDelta {
        delta = (data - prevData).abs.sum/3;
        prevData = data.copy;
    }

    prCreateTask {
        task = TaskProxy.new({
            inf.do {
                this.prCalculateDelta;
                resamplingFreq.reciprocal.wait;
            }
        }).play;
    }

    trace {|t=true|
        if(t){ 
            oscFuncTrace = OSCFunc({|oscdata|
                postf("x: %, y: %, z: %, d: %\n", x.trunc(0.01), y.trunc(0.01), z.trunc(0.01), delta.trunc(0.01));
            },path: oscPath, recvPort: port);
            }{
                oscFuncTrace.clear;
            };
    }

    free {
        task.clear;
        oscFunc.clear;
        oscFuncTrace.clear;
    }
}
