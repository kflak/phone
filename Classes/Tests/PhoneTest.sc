TestPhone : UnitTest {
	test_check_classname {
		var result = Phone.new;
		this.assert(result.class == Phone);
	}
}

TestSetXbus : UnitTest {
    var phone, server;
    setUp{
        phone = Phone.new;
        phone.x = 1.0;
    }

    test_set_xbus {
        var result = phone.xbus.getSynchronous;
        this.assert(result == 1.0);
    }

    tearDown{
        phone.free;
    }

}
PhoneTester {
	*new {
		^super.new.init();
	}

	init {
		TestPhone.run;
        TestSetXbus.run;
	}
}
