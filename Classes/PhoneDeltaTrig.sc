PhoneDeltaTrig {
    classvar <>resamplingFreq = 20;
    
    var <phone;
    var <>threshold;
    var <>speedlim;
    var <>minAmp;
    var <>maxAmp;
    var <function;
    var <task;
    var <deltaFunc;
    var <>debug = false;

    *new{|phone, threshold=0.1, speedlim=0.5, minAmp=0.0, maxAmp=0.3, function|
        ^super.newCopyArgs(phone, threshold, speedlim, minAmp, maxAmp, function).init;
    }

    init{
        if(phone.class != Phone){
            "Please specify a Phone! Aborting.".error;
        }
    }

    set {|...args|
        if(args.size.even){
            args.pairsDo{|param, value|
                switch( param,
                    \threshold, { threshold = value },
                    \speedlim, { speedlim = value },
                    \minAmp, { minAmp = value },
                    \maxAmp, { maxAmp = value },
                )
            }
        }{
            "Odd number of arguments. Aborting".error; 
        }
    }

    prCreateTask {
        task = TaskProxy.new({
            var free = true;
            inf.do({
                var dt = phone.delta;
                if(free){
                    if(dt > threshold){
                        function.value(dt, minAmp, maxAmp);
                        if(debug){dt.postln};
                        free = false;
                        SystemClock.sched(speedlim, {
                            free = true;
                        });
                    };
                };
                resamplingFreq.reciprocal.wait;
            })
        })
    }

    play {
        task.stop;
        if (phone.notNil){
            this.prCreateTask;
            task.play;
        }{
            "No Phone specified. Aborting".error;
        }
    }

    free {
        task.stop;
        task = nil;
    }
}
